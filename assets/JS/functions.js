// // function declaration
// function display(name){
//     console.log(`Welcome ${name}`);
// }

// // total = a+b (Expression)
// // Function Expression
// // named Expression
// let name =function userName(name){
//     console.log(`Welcome ${name}`);
// };
// // anonymous Expression
// {let name = (name)=>{
//     console.log(`Welcome ${name}`);
// };}
// userName("Arun");

// // Js Hoisting
// add(50,150);
// function add(no1,no2){
//     console.log(no1+no2);
// }


// let number = (no1,no2)=>{
//     console.log(no1, no2);
//     console.log(no1+no2);
// };

// number(50);

// let name ="Arunkumar";
// console.log(name);

// IIFE (Immediately Invoked Function Expression)

// (function add(no1,no2,arguments){
//     console.log(arguments);
//     console.log(no1+no2);
// })(10,20,30,40);

// function studentsMark(){
//     let total=0;
//     for(let mark of arguments){
//          total += mark;
//     }
//     return total
// }

// let result = studentsMark(50,20,60,45,75);

// console.log(`Your Mark is : ${result}`);


// Rest Operator
// syntax: ...args


// function studentsMark(mark1,mark2,mark3,...marks){
//     console.log(...marks);
//     let total=0;
//     total = mark1+mark2;
//     return total
// }

// let result = studentsMark(50,20,60,45,75,45);

// console.log(`Your Mark is : ${result}`);

// function billData(total,tax = 18){
//     totalAmount = total+tax;
//     console.log(`Total Amount: ${totalAmount}`);
// }

// billData(1000,25);

// function fullName(firstName, lastName =""){
//     console.log(`${firstName} ${lastName}`);
// }

// fullName("Arunkumar")

// Getter and Setter
// Data property
// const personData={
//     firstName: "Arunkumar",
//     lastName:"Selvam",
//     greeting(){
//         console.log(`My name is ${this.firstName} ${this.lastName}`);
//     }
// }

// personData.greeting();
// personData.firstName = "Prasanna";


// // Accessor Properties
// const person = {
//     firstName: 'John',
//     lastName: 'Doe',
    
//     get fullName() {
//         return `${this.firstName} ${this.lastName}`;
//     },
//     set fullName(value) {
//         const [firstName, lastName] = value.split(' ');
//         this.firstName = firstName;
//         this.lastName = lastName;
//     }
// };

// console.log(person.fullName); // Output: John Doe

// person.fullName = 'Alice Smith';
// console.log(person.fullName); // Output: Alice Smith



// Getter

let employeeData = {
    id:1,
    firstName:"Arunkumar",
    lastName:"Selvam",
    role:"Designer",
    get greeting(){
        console.log(`Welcome to our organization ${this.firstName} ${this.lastName}.`);
    },
    set greeting(val){
        let names= val.split(" ");
        this.firstName = names[0];
        this.lastName = names[1] ?? "";
    }
}

employeeData.greeting ="Prasanna";
employeeData.greeting;

// nullish operator

// true ?? false

// Scopes

// Global
// local 
// let offer = 20;

// console.log(offer);

// function offerStatus(){
//     let offer1 = 30;
//     console.log(offer1);
// }

// // console.log(offer1);

// offerStatus();


function add(no1,no2,arguments){
    console.log(arguments);
    console.log(no1+no2);
}

add(20,50,30,60);