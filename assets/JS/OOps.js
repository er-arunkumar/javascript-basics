// Object Creation

// let oopsObj = {
//     topic: "OOps",
//     oopsFn(name){
//         console.log("Hello"+name +" "+ variable1 +"sdagagaha"  );
//     }
// }


// oopsFn("Faiyz");

// console.log(oopsObj);

// Template Literals
let name="Arun";
let favColor = "Blue";

//Tilda - backtick

console.log(`Welcome to ${name}.  My favorite color is ${favColor}.`);


// factory function

function createStudent(name){

    return{
        name,
        standard:"12",
        // Method Declaration
        greeting(){
            console.log(`Welcome to ${this.name}. My standard is ${this.standard}.`);
        }
    }
}

// Student(name){
//     this.name = name,
//     this.standard ="12",
//     // Method Declaration
//     greeting = function(){
//         console.log(`Welcome to ${this.name}. My standard is ${this.standard}.`);
//     }
// }
// function calling
let students = createStudent("Faiyz","che");
// method calling
students.greeting();


// constructor function

function Student(name){
        this.name = name,
        this.standard ="12",
        // Method Declaration
        greeting = function(){
            console.log(`Welcome to ${this.name}. My standard is ${this.standard}.`);
        }
}
// object creation
let student = new Student("Faiyz");
// method calling
// student.greeting();

//Object literals
// let numbers = {}; // ===> let student = new Object();

// //String literals
// let city = "Chennai"; // ===> let city = new String("Chennai");

// independent nature

let x = 10;
let y= x;

console.log(x,y);
x=20;
console.log(x,y);
y=x;
console.log(x,y);

// Reference type

let num01 ={value:10}; //memory address
let num02 = num01;

num01.value = 20;

console.log(num01.value , num02.value);

// Object

let place = {}; // ==> let place = new Object();

// Object _ Inbuild Methods
console.log("Object Inbuild Methods");

let studentData = {name:"Arun", id:"001",city:"Thanjavur"}

console.log(studentData);

// for (let key in studentData)
//     console.log(key, studentData[key]);

//let x= {}; let x = new Object();
// for (let key of Object.keys(studentData));
//     console.log(key);

// for (let key of Object.entries(studentData));
//     console.log(key);

// Object Cloning
console.log("Object Cloning");

console.log(studentData);

let studentData1 ={};

for (let key in studentData1){
    studentData1[key] = studentData[key];
    console.log("Other Object : ", studentData1);
}
    
// assign()

let  studentData2 = Object.assign({age:29},studentData);
console.log("Assign Method Clone Object: ",studentData2);

// spread Operator
let studentData3 ={...studentData2};
console.log("Spread Operator: ", studentData3);

studentData3.isYoung = true;
console.log("Spread Operator: ", studentData3);

// Object --> Yes
// String
// Boolean
// Number

// String Inbuilt function
let firstName = "Arunkumar";
let lastName = "Selvam";

console.log(firstName.length);

//concat
console.log(firstName +" "+ lastName);
console.log(firstName.concat(" ",lastName));

console.log(firstName.toLowerCase());
console.log(firstName.toUpperCase());

// Math()

console.log(Math.round(5.6));
console.log(Math.ceil(5.2));
console.log(Math.floor(5.9));


// Date()

console.log(Date());

const date = new Date();

console.log(date.getFullYear());