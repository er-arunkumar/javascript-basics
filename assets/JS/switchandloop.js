// Switch Case
// Grading System

let mark = 91;
let grade;

switch (true) {

    case mark>80:
        console.log("Good, Grade : A");
        break;
    case mark>90:
        console.log("OutStanding, Grade : O");
        break;
    case mark<60:
        console.log("Fail");
        break;
    case mark>60:
        console.log("Just Pass, Grade : C");
        break;
    case mark>70:
        console.log("Average, Grade : B");
        break;

    // case "O":
    // case "A":
    // case "B":
    // case "C":
    //     console.log("Pass");
    //     break;

    default:
        console.log("Enter valid Grade");
}

// Loops
// While

// variable initialization (global variable)
//     while (condition) {
//         // Code to be executed as long as the condition is true
// 		step (increment or decrement)
//     }

// print 0-10 (0, 2,4,6,8,10)
let number= 0; // Initialization
console.log("While Loop:");
while (number <=10){ 
    if( number%2 ==0)
    console.log("Number #",number);
    number++; //number+=2
}

// Do-While

// variable initialization (global variable)
//      do{
//         // Code to be executed as long as the condition is true
// 		step (increment or decrement)
//     }while (condition);

let num= 1; // Initialization
console.log("Do-While Loop:");
do{ 
    console.log("Number #",num);
    num++;
}while (num <=10);

// for loop
// for (initialization (local); condition; step) {
//     // Code to be executed for each iteration
// }
console.log("For Loop:");

for(let value=1;value<=10;value++){
    console.log("Number #",value);
}

// for-in (objects and arrays)


// for(variable in object){
//     // set of code
// }
console.log("For-in");
let candidate=[
   "Faiyz",
    29,
    true,
];

for (let value in candidate){
    console.log(value);
}

//for-of
// for(variable of object){
//     // set of code
// }
console.log("For-of");
let candidate1=[
    "Faiyz",
     29,
     true,
 ];
 
 for (let value of candidate1){
     console.log(value);
 }