// alert("Arunkumar Selvam");
// console.log("Welcome to JS");
// JavaScript - Single line comment
/* code -
 Multiline comment
 */  

//  Variables

/* 
variable syntax

    variableType variableName = value;
    
*/
// let firstName = "Arunkumar"; // Variable Declariation
// let age = 25; // dynamic typed 
// firstName="shafik"; 

// const nativePlace = "Trichy";
// const pi = 3.14;


// alert(firstName);

// console.log(firstName);
// console.log(nativePlace);

let gender = "Female";

console.log(typeof gender);

let returns1 = "arun";

let Returns2 = "kumar";

let fullName;


const apex = 27.502;
console.log(typeof apex);

let dates;
console.log(dates);

let status = null;
console.log(status);

status = "active";

console.log(status);

// object

// variableType objectName = {};

let employee1 = {}; // Empty Object

let studentDetails={
    //key : value

    name:"Arunkumar",
    age: 25,
    isYoung: true,
    status:null,
    parentsDetais: {
        fatherName:"Selvam"
    }
}

console.log(typeof employee1);

console.log(studentDetails);

console.log(studentDetails.name);

console.log(studentDetails.status);

console.log(studentDetails['age']);

console.log(studentDetails.parentsDetais.fatherName);

studentDetails.name = "Faiyz";

console.log(studentDetails);


// array

let favFoods = ["Biriyani", "Dosa", "Idle", "Rosemilk"];


console.log(typeof favFoods);

console.log(favFoods);

console.log(favFoods[0]);

favFoods[2] = "chicken65";

favFoods[5] = "Icecream";

console.log(favFoods.length);


































