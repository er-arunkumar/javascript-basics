// functions

// syntax

// function declaration
function functionName(){
    // set of code
}
// function calling
functionName();


function call(){
    let name ="selvanayaki";
    console.log(name);

}

call();

let studentObj ={}

// console.log(studentObj);

function studentGreeting(fname,lname){
    let welcomeNote = "Welcome " + fname  +" "+lname+ " Have a nice day!";
    console.log(welcomeNote);
}

studentGreeting("ArunKumar", "Selvam");

// Increament 

let number1 = 10;

console.log(number1); //10
console.log(++number1); //11
console.log(number1);//11

let number2 = 5;

console.log(number2);
console.log(--number2);
console.log(++number2);
console.log(number2--);
console.log(number2);

// Comparison Operator

// Equality Operator
let num1 = 10; // number
let num2 = 5; // string

console.log(num1 !== num2); //true (check value)
console.log(num1 === num2);//false (check value and datatype)

// Relational Operator
let number01 = 5;
let number02 = 5;

console.log("This relational operator "+ (number01 >= number02));


// String Comparison
console.log(5 <= "5");

// locial operators

// && - AND (2 condition True)
// || - OR (any 1 condition True)
// ! - NOT

// Conditional Statements
// if-else

// if (condition){ // true
//     // set of code
// }
// else if (condition){
    // set of code
// }
// else{
//     // set of code
// }

// If above age 18 and voter list name
let voter = {
    age:"",
    listName:true,
    postalVote: true
}

if(voter.age>18 && voter.listName==false){
    console.log("Elligible");
}
// else if(voter.age>18 && voter.postalVote==true){
//     console.log("Elligible for Postal Vote ");
// }
else{
    console.log("Not Elligible");
}

// Ternary Operator

// let variableName = (condition) ? "block1": "block2";

let voterElligible = (voter.age>18 && voter.listName==true) ? "Elligible" : "Not Elligible";

console.log(voterElligible);

// JS Mechanism (Truthy or Falsy)

// Truthy

// Falsy 

// undefined
// null
// 0
// false
// '' and ""
// NaN

let numX = 10;
let numY="10";

console.log(numX == numY)

// Short Circuiting

// operand1 (operator) operand2 (operator) operand3

function operation(){

    let result = 10+(5*2); // 25 25 20

    console.log(result);
}

operation();

condition ? "block1" : "block2";