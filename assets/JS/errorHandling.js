// Error Handling
// Try,Throw,Catch

const passangerData = {
    firstName:"Rajini",
    lastName:"Kanth",
    get journeyGreeting(){
        alert(`Happy Journey ${this.firstName} ${this.lastName}`);
    },
    set journeyGreeting(value){
        if(typeof value != "string"){
            let err = new Error("Please,enter vaild name. Only using letters.");
            throw err;
        }

        let userName = value.split(" ");
        this.firstName = userName[0];
        this.lastName = userName[1];

    }
}
try{
    passangerData.journeyGreeting = 35;
}
catch(err){
    console.log(err);
    alert(err);
}

passangerData.journeyGreeting;