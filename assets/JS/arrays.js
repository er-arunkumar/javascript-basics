// let rice = {
//     brandName: "XYZ",
//     price:45,
//     qty:"1kg"
// }

// let oil = {
//     brandName: "XYZ",
//     price:60,
//     qty:"1lt"
// }

// let pen = {
//     brandName: "XYZ",
//     price:5,
//     qty:1
// }

// let listOutItems = [rice,oil];

// console.log(listOutItems);

// //JS Inbuilt Method
// let numbers = [1,2,3];

// console.log(numbers);
// // Push - backside add 
// numbers.push(4,10);
// console.log(numbers);

// // Unshift - frontside add
// numbers.unshift(-1,0);
// console.log(numbers);
// // Splice 
// numbers.splice(4,2,5,6,7,8,9);
// console.log(numbers);

// // Function
// function arrayOperation(){
//     // set of code
// }

// // Arrow Function
// const functionName = () => {
//     // set of code
// }

// // Finding of Array
// let riceVarities = ["pooni rice", "biriyani rice", "idle rice","pooni rice"];

// // indexOf
// console.log(riceVarities.indexOf("pooni rice"));

// // lastIndexOf
// console.log(riceVarities.lastIndexOf("pooni rice"));

// // includes
// console.log(riceVarities.includes("IR8"));


// //find

// let employeeNames = [{name:"Arunkumar"}, {name:"Faiyz"}, {name:"Mercy"}, {name:"Selvanayagi"}]

// console.log(employeeNames.findIndex((key)=>{return key.name === "Faiyz"}));

// //JS Inbuilt Method
// let number = [1,2,3];

// console.log(number);
// // Pop - backside add 
// number.pop(2);
// console.log(number);

// // Unshift - frontside add
// number.shift(0);
// console.log(number);
// // Splice 
// number.splice(0,1);
// console.log(number);

// // Emptying Array

// let arrayName = [1,3,5,7,9]; // Memory #1
// console.log(arrayName);
// console.log(arrayName.length);
// //[]
// arrayName=[]; // Memory #2
// console.log(arrayName);

// console.log(arrayName.length = 0);



// function User(name,city)
//         {
            
//                 this.name=name,
//                 this.city=city,
//                 //method declaration
//                 greeting()
//                 {
//                 console.log(`Welcome to ${this.city},${this.name}`);
//                 }
            
            
            
//         }
//         let student= new User("Mercy","singapore");
//         student.greeting();

// reference type
// let info = {name:"Arun", age:24};
// info.isYoung = true;
// console.log(info);
// let info2={name:"Prasanna", age:21};

// let x=[info,info2];

// let info3 ={name:"Faiyz",age:29};
// x.unshift(info3);
// console.log(x);
// x.shift(info3);
// console.log(x);

// function 

// functionName() => {
//     //set of code
// }

// functionName();

// let functionName = () => {
//     //set of code
// }

// let animals1=["tiger","lion","cat"];
// let animals2=["monkey","horse","dog"];

// // // concat()
// // let cage= animals1.concat(animals2);

// // console.log(cage);

// // // slice()
// // let animals3 = cage.slice(0,1);
// // console.log(animals3);

// let cage =[...animals1, ...animals2];

// console.log(cage);

// // Foreach() (Iterating)
// let x={subject:"tamil"};
// x.mark=60;
// let groupSubjects = [ x, "English",["biology", "computer science"], "maths", "physics", "chemistry"];

// console.log(groupSubjects[0]);;
// groupSubjects.forEach((group) => {
//     console.log(group);
// });

// sorting

// let numbers = [0,5,3,1,4,2];

// // Assending Order
// let assending = numbers.sort();
// console.log("Assending Order:",assending);
// // reverse
// console.log("Dessending with reverse",assending.reverse());;
// // // deassending Order
// console.log("deassending Order: ",numbers.sort((a,b)=>{
//     return b>a;
// }));

// // reverse

// // Reference type sorting
// let heros = [{id:1,name:"Rajini",latestMovie:"jailer"},
// {id:2,name:"@vijay",latestMovie:"leo"},
// {id:3,name:"ajith",latestMovie:"mankatha"}
// ];

// console.log(heros.sort((a,b)=>{
//     if (a.name < b.name) 
//     {return -1}
//     if (a.name > b.name)
//     {return 1} 
//     return 0
// }));

// Reduce 
// const array1 = [1, 2, 3, 4];

// // 0 + 1 + 2 + 3 + 4
// const initialValue = 0;//1
// const sumWithInitial = array1.reduce(
//   (accumulator, currentValue) => accumulator + currentValue,
//   initialValue,
// );

// console.log(sumWithInitial);
// Expected output: 10

// // Mapping
// let numbers = [1,2,3,4,5];

// let squareValues = numbers.map((number)=>{
//     return number*number*number
// });
// console.log(numbers);
// console.log(squareValues);

// // Reference type map
// let userData = [{id:1, firstName:"Arunkumar",lastName:"Selvam"}];

// let fullName = userData.map((value) =>{
//     return [value.firstName,value.lastName].join(" ");
// });

// console.log("Full Name: ",fullName[0]);

// filter

let personAge = [ 18, 36,25,50,80,25];

let userResult = personAge.findIndex((age)=>{
    return age === 25;
});

console.log(userResult);

//function Declaration
// function functionName(){
//     //set of code
// }

// functionName();

// // () =>{

// // }

// // objectName:
// // {
// //     function functionName(){
// //         //set of code
// //     }
// // }

// // Function Expression
// let variableName = ()=>{};
// Javascript Hoisting
testOff();
function testOff(){
    console.log("Normal testOff function.");
}


let testOff2 = ()=>{
    console.log("Normal testOff2 function.");
}
testOff2();

