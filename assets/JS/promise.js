const data = new Promise((resolve, reject) =>{
    //setTimeout(()=>{}, second)
    setTimeout(()=>{
        console.log("First");
        const ans = "Arun";
        if(ans){
            resolve(ans);
        }
        else{
            reject(new Error("No data"));
        }

    },5000)
});

//then() -> success receive
//catch()-> reject receive
// data.then((result)=>{
//     console.log(result);
// }).catch((error)=>{
//     console.log(error.stack);
// })
async function fetchData(){
    try{
        const name = await data;
        console.log(name);
    }
    catch(error){
        console.log(error.stack);
    }
}


console.log("last");


