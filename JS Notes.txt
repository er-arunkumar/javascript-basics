Javascript

https://survey.stackoverflow.co/2023/

What is javascript?
- JavaScript is a dynamic programming language that's used for web development,It is used both on the client-side and server-side that allows you to make web pages interactive.
- HTML and CSS are languages that give structure and style to web pages, JavaScript gives web pages interactive elements that engage a user.
- It is an interpreted programming language with object-oriented capabilities 

Client-side Server:
- Client-side refers to the part of a website or application that runs on your device, like your computer or phone. 
- It's where your browser takes care of things like displaying content, running animations, and handling user interactions.

Server-side Server:
Server-side refers to the part of a website or application that runs on the server, which is a powerful computer that stores and manages data. 
It handles tasks like processing form submissions, managing user accounts, and retrieving information from a database.

Interpreted Language:
An interpreted language runs code line by line as it's read. 
Ex: Javascript, Python and Ruby

Compiled Language:
A compiled language translates the entire code into machine language before running.
Ex: Java, C, C++

Who discovered by JS?

- JS was created by Brendan Eich.
- Eich worked at Netscape and implemented JavaScript for their web browser, Netscape Navigator.
- It code name was mocha, then next version netscapt navigator 2.0 called LiveScript.
- In Netscape Navigator 2.0 beta 3 it got its final name, JavaScript.

Why learn JS?

- It is a light-weighted and interpreted language
- All popular web browsers support JavaScript
- JavaScript is an object-oriented programming language
- Better community support
- High Demand

JS Frameworks:

- React JS (Libray)
- Angular JS
- Vue JS and etc.,

Browser JS Engine:

V8 (used in Google Chrome and Chromium-based browsers) 
SpiderMonkey (used in Mozilla Firefox)
JavaScriptCore (used in Safari)

Node JS:

- Node.js is an open-source and cross-platform JavaScript runtime environment
- It was initially written by Ryan Dahl (scientist) in 2009 
- Node.js allows developers to use JavaScript for server-side scripting. This means you can write both the front-end and back-end of web applications in JavaScript
- Node.js introduced a package manager called npm. This allows programmers to publish and share Node.js packages, simplifying the installation, update, and uninstallation of packages
- He was motivated to create a lightweight and efficient server-side technology that could handle large-scale and real-time applications

Standardizing JavaScript ECMA

ECMA - European Computer Manufacturers Association

- It is specification. specification means set of rules and regulations.
- JavaScript is based on ECMAScript.
- ECMAScript releasing version(v1) in 1997. current version is (ES14) in 2023.

Write a Javascript code in 2 ways on HTML Page

1. Internal JS

<script>
	// JS Code
</script>

2. External JS (Most Preferred)

- Create a new file in the format filename.js, such as index.js, and write your JS code without using the <script></script> tags.
- Link this file to your HTML page using the <script src="filepath"></script> tag.

JS Comments:

// - Single Line Comments
/**/ - Multi Line Comments

How to run the JS file in NodeJS?

open terminal
write a command 
	
	node filename.js
	
JS Variables:

- A variable is like a container that holds values or store the value
- These values can be of various datatypes such as numbers, strings, or booleans etc.,

Syntax:

	variableType variableName = value;

JS Types of Variabls:

var - intially used
let and const - After ES6+ released add the let and const method

var:
Use the reserved keyword "var" to declare a variable in JavaScript.

- var can be Redeclared and Reassigned.
- var have Global Scope.

String: 
A string in JavaScript is a sequence of characters, like letters, numbers, or symbols, enclosed in single ('') or double ("") quotes. 
It's used to represent text or data.

Ex: "Hello, world!" or '12345' 

Concatenation:
Concatenation is the process of combining strings together. 
In JavaScript, you can use the + operator to concatenate strings.

String + String = Concatenation

ex: "Welcome"+"Javascript" (2 string)
"Welcome"+name (string + variable)

JS is a dynamically typed language.

Variables Naming Convention:

1. No Javascript keywords in variable names
2. Should not start with numbers
3. Should not use Space and - 
4. Variable names are case-sensitive
5. Use meaningful name
6. Variable names using camelCase method

Ex: firstName, lastName

let:

- let cannot be Redeclared.
- let must be Declared before use.
- let have Block(Local) Scope. inside the {} only accessed.

const:

- const cannot be Redeclared.
- const cannot be Reassigned.
- const have Block(Local) Scope. inside the {} only accessed.

JS DataTypes:

 - Primitive Type

    String, Integer or Number, boolean, undefined(default) and null.
 
 - Reference Type

    Objects, array and functions

How to find the datatype in js?

    typeof variableName;

JS Objects:

	- An object is a collection of key-value pairs stored in a single variable.
	- The keys in an object are used to access the values, and the keys and values can be of any data type.
	- Objects are commonly used to store related data, such as a collection of properties for a single item, or a collection of methods for an object.
	- Objects are defined using curly braces, with keys and values separated by colons.

Object Declare:
	- To declare an object in JavaScript, use curly braces {}
	
	Ex: let person = {}; // Empty object declaration

Object Accessing:
	There are 2 types
	- Access properties of an object using dot notation (object.property)
	- Access properties of an object using bracket notation (object['property'])
	
	Ex: let person = { name: "John", age: 30 };
	console.log(person.name); // Accessing property "name" using dot notation
	console.log(person['age']); // Accessing property "age" using bracket notation

Value Change in Object:

	- Assign a new value to a property of an object using dot notation or square brackets (object.property = value (or) object['property'] = value)
	
	Ex: let person = { name: "John", age: 30 };
	person.age = 35; // Changing value of property "age"

Nested Objects:

	- Objects can contain other objects as properties.
	
	Ex: let person = {
			name: "John",
			address: {
				street: "123 Main St",
				city: "Anytown"
			}
		};
		console.log(person.address.city); // Accessing nested property "city"

What is array?

    - An array in JavaScript is an object that stores multiple values simultaneously, similar to a list.
    - It's a global object capable of holding various data types.
    - Arrays are zero-based, meaning the first element's index is 0.
    - Elements within an array can be of any type, including strings, booleans, objects, or even other arrays.
    - JavaScript provides built-in functions, called methods, to manipulate arrays.
    - These methods are part of the Array.prototype object and can be used with any array in JavaScript.
	
Array Declaration: Use square brackets [] to declare an array
Ex: 
	let numbers = []; // Empty array declaration

Array Accessing: Access elements of an array using square brackets and the index (array[index]).
Ex:
	let numbers = [10, 20, 30];
	console.log(numbers[0]); // Accessing element at index 0

Array value Update: Assign a new value to an element of an array using square brackets and the index (array[index] = value).
Ex:
	let numbers = [10, 20, 30];
	numbers[1] = 25; // Updating value at index 1

Nested Array: Arrays can contain other arrays as elements.
Ex:
	let matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
	];
	console.log(matrix[1][2]); // Accessing element at row 1, column 2

Array Properties: Arrays have properties like length(), map(), push(), pop(), etc.,

length - find the array length
push() - adds one or more elements to the end of an array
pop() - removes the last element from an array

Operators:

	variable + operator = expression

Arithmetic Operator: Arithmetic operations are used to perform mathematical calculations and manipulate numerical values. The basic arithmetic operations include +, -, *, /, %, **, increment(++) and decrement(--)
Assignment Operator: Assignment operators are used to assign values to variables. The basic assignment operator is the "=" sign, which assigns the value on the right side of the operator to the variable on the left side
Comparison Operator: Comparison operators are used to compare two values and return a Boolean value (true or false) depending on the result of the comparison. There are 2 types.

	Relational Operator:relational operators that can be used to compare values and determine the relationship between them. When using these operators, the expressions on either side of the operator will be evaluated and compared to determine if the relationship between them is true or false
	 
		> - greater than
		< - less than
		>= - greater than or equal to
		<= - less than or equal to
		
	Equlity Operator: 
	
		Equality (==): This operator compares two values to see if they are equal.
		Equality (===): The other hand, is a strict equality operator. It will only return true if the values being compared have the same type and value.
		Inequality (!=): This operator compares two values to see if they are not equal.

Logical Operator:

&& - operator compares two expressions and returns true only if both expressions are true. 
|| - operator compares two expressions and returns true if either or both expressions are true. 
!  - operator negates the value of a single expression, so if the expression is true, it returns false and if the expression is false, it returns true.

Ternary Operator:

Ternary operators in JavaScript provide a concise way to write conditional statements. 
The ternary operator is also known as the conditional operator, and it allows you to evaluate a condition and return one of two expressions based on whether the condition is true or false.

Syntax

             condition ? expression1 : expression2;

The condition is evaluated.
If the condition is true, the value of expression1 is returned.
If the condition is false, the value of expression2 is returned.

If Statement:

The "else if" statement is used to implement multiple conditional branches in the decision-making process. It allows you to check for additional conditions after the initial "if" condition, and if the previous "if" condition is false, it evaluates the "else if" conditions one by one until it finds a true condition or reaches the "else" block.

Syntax :
          if (condition1) {
                   // Code to be executed if the condition1 is true
         } else if (condition2) {
                   // Code to be executed if condition2 is true
         } else if (condition3) {
                   // Code to be executed if condition3 is true
         } else {
                   // Code to be executed if none of the above conditions are true
         }

Switch Statement:

The "switch" statement provides a way to execute different blocks of code based on the value of an expression. It offers an alternative to multiple nested "if-else" statements when you have several possible conditions to check.

Syntax:
    switch (expression) {
        case value1:
            // Code to be executed if the expression is equal to value1
            break;
        case value2:
            // Code to be executed if the expression is equal to value2
            break;
            // More cases can be added here
        default:
            // Code to be executed if the expression does not match any of the cases
            break;
    }

break is a keyword to stop the program.

loop:

A loop in programming is a way to repeat a block of code multiple times.
It allows you to execute the same set of instructions over and over again until a certain condition is met.

Initialization: This part is executed once before the loop starts. It is typically used to initialize a loop control variable.
Condition: The loop continues as long as the condition is true. Before each iteration, the condition is evaluated. If the condition is false, the loop terminates, and the program continues with the next statement after the loop.
Step: This part is executed after each iteration of the loop. It is typically used to update (either increment or decrement) the loop control variable.

While:
It is a control flow statement that allows you to repeatedly execute a block of code as long as a specified condition evaluates to true. It is one of the fundamental loop constructs in JavaScript and is used when the number of iterations is not known beforehand.

Syntax:
	variable initialization (global variable)
    while (condition) {
        // Code to be executed as long as the condition is true
		step (increment or decrement)
    }
	
Do-while:
It is a control flow statement that allows you to repeatedly execute a block of code as long as a specified condition is true. It is similar to the "while" loop, but with one key difference: the "do-while" loop guarantees that the code inside the loop is executed at least once, even if the condition is false from the beginning.

Syntax:
	   variable initialization (global variable)
       do {
           // Code to be executed
		   step (increment or decrement)
       } while (condition) ;
For:
It is a control flow statement that allows you to repeatedly execute a block of code for a specific number of iterations. It is one of the most commonly used loops and is particularly useful when the number of iterations is known or when you want to iterate over elements in an array or other data structures.

Syntax:
       for (initialization (local); condition; step) {
           // Code to be executed for each iteration
       }

For-in:
It is a traditional iteration method used to loop over the properties of an object. It allows you to iterate through the keys or property names of an object, rather than the values. The "for...in" loop is particularly useful when you need to access the keys or properties of an object and perform operations on them.

Syntax:
       for (variable in objectName) {
              // Code to be executed for each property of the object
       }
	   
	Ex:
		const person = {
			name: "Arunkumar",
			age: 25
		};
		
		for(let key in person) {
			console.log(person[key])
		}
For-of:
It is a modern iteration method introduced in ECMAScript 6 (ES6) that allows you to loop over iterable objects, such as arrays, strings, sets, maps, and more. The "for...of" loop provides a more concise and readable syntax for iterating over elements compared to traditional "for" loops.

Syntax:
       for (variable of array) {
              // Code to be executed for each iteration
       }
	   
	   Ex:
	   let colors = ['red', 'blue', 'green'];

		for(let color of colors){
			console.log("Color: " + color);
		}


OOPs in JS:

Template literals:

	- Template Literals use back-ticks (` `) rather than the quotes (" ") to define a string
	- With template literals, you can use both single and double quotes inside a string.
	- Template literals allows multiline strings
	- In expression denotes ${variableName}

Ex: `My name is ${name}`

Factory Function:

A factory function is a function that creates and returns objects without using the new keyword.
Each time the factory function is called, it creates a new object instance with the specified properties and methods.
Flexibility: Factory functions allow for the creation of multiple similar objects with different property values, making them versatile for creating object instances dynamically.

Note: functionName should be camelCase

syntax:

function functionName(){
	return{
		//object creation
		//method declaration (not necessary)
	}
}

let variableName = functionName(); // function calling
variableName.methodName(); // method calling

Constructor Function:

A constructor function is a special function used to create and initialize objects created with the new keyword.

Note: functionName should be PascalCase

Ex:

function Person(name, age) {
    this.name = name; // current instance
    this.age = age;
    this.greet = function() {
        console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
    };
}

let objectName = new functionName(); // object creation
objectName.methodName(); // method calling

this: In JavaScript, this refers to the current instance of the object. Inside a constructor function, this refers to the object being created by that constructor.

Object Literals: let x = {} ===> let x = new Object(){};
String Literals: let name = "Arun"; ===> let name = new String("Arun");
Number Literals: let age = 25; ===> let age = new Number(25);
Boolean Literals: let isYoung = true; ===> let isYoung = new Boolean(true);

Primitive Datatypes are independent variables.

Object Inbuilt Functions:

Some commonly used built-in methods for JavaScript objects.

syntax:
		Object.methodName();

list of methods:

	- keys,
	- values,
	- entires,
	- assign, etc.,

Object.keys():Returns an array of a given object's own enumerable property names.

    Ex:
		const person = { name: 'Alice', age: 30 };
		const keys = Object.keys(person);
		console.log(keys); // Output: ['name', 'age']

Object.values():Returns an array of a given object's own enumerable property values.
   
   Ex:
		const person = { name: 'Alice', age: 30 };
		const values = Object.values(person);
		console.log(values); // Output: ['Alice', 30]

Object.entries():Returns an array of a given object's own enumerable property key-value pairs.
    
	Ex:
		const person = { name: 'Alice', age: 30 };
		const entries = Object.entries(person);
		console.log(entries); // Output: [['name', 'Alice'], ['age', 30]]

Object.assign(): Copies the values of all enumerable own properties from one or more source objects to a target object and returns the target object.
    
	Ex:
		const target = { a: 1, b: 2 };
		const source = { b: 3, c: 4 };
		const merged = Object.assign(target, source);
		console.log(merged); // Output: { a: 1, b: 3, c: 4 }
		
Object Cloning:
Object cloning refers to the process of creating a copy of an object. This allows you to duplicate an object's properties and values without modifying the original object.

Types of Object Cloning:

Shallow Cloning: Shallow cloning creates a new object and copies all the properties of the original object. However, if the original object contains nested objects, only references to those nested objects are copied, not the nested objects themselves.

Shallow cloning using three different methods: for...in loop, Object.assign(), and spread operator (...).

Shallow Cloning with for...in loop (traditional method):

    Syntax:
		function shallowClone(originalObject) {
			let clonedObject = {};
			for (let key in originalObject) {
				clonedObject[key] = originalObject[key];
			}
			return clonedObject;
		}

	Example:
		let studentData1 = {name:"Guru", age:29, isYoung:true}
		let studentData2 = {};

		for(let key in studentData2) {
			studentData2[key] = studentData1[key];
		}

Shallow Cloning with Object.assign():

    Syntax:

		const clonedObject = Object.assign({}, originalObject);

	Example:
		const originalObject = { name: 'Alice', age: 30 };
		const clonedObject = Object.assign({}, originalObject);

Shallow Cloning with Spread Operator (...):

    Syntax:

		const clonedObject = { ...originalObject };

	Example:

		const originalObject = { name: 'Alice', age: 30 };
		const clonedObject = { ...originalObject };

Math Object:
Math.random():Generates a random floating-point number between 0 (inclusive) and 1 (exclusive).
        
	Syntax:
		const randomNumber = Math.random();

	Example:
		const randomNumber = Math.random();
		console.log(randomNumber); // Output: 0.12345 (random number between 0 and 1)

Math.floor():Returns the largest integer less than or equal to a given number.
    
	Syntax:

		const roundedNumber = Math.floor(3.6);

	Example:
			const roundedNumber = Math.floor(3.6);
			console.log(roundedNumber); // Output: 3

Math.ceil():Returns the largest integer greater than or equal to a given number.
    
	Syntax:

		const roundedNumber = Math.ceil(3.6);

	Example:
			const roundedNumber = Math.ceil(3.6);
			console.log(roundedNumber); // Output: 4

String Object:

String.length: Returns the length of a string.

    Syntax:
		const str = 'Hello';
		const length = str.length;

	Example:
		const str = 'Hello';
		const length = str.length;
		console.log(length); // Output: 5

String.toUpperCase():Converts a string to uppercase letters.
    Syntax:

		const str = 'hello';
		const upperCaseStr = str.toUpperCase();

	Example:

			const str = 'hello';
			const upperCaseStr = str.toUpperCase();
			console.log(upperCaseStr); // Output: 'HELLO'

Date Object:

new Date():Creates a new Date object representing the current date and time.
    
	Syntax:
		const currentDate = new Date();

	Example:
		const currentDate = new Date();
		console.log(currentDate); // Output: Current date and time

Date.getMonth():Returns the month (from 0 to 11) for the specified date.
    
	Syntax:
		const currentDate = new Date();
		const currentMonth = currentDate.getMonth();

	Example:
        const currentDate = new Date();
        const currentMonth = currentDate.getMonth();
        console.log(currentMonth); // Output: Current month (0 to 11)
		
Adding Elements to an Array:

    push():Adds one or more elements to the end of an array.
    Syntax:
		array.push(element1, element2, ...);

	Ex:
		const fruits = ['apple', 'banana'];
		fruits.push('orange', 'kiwi');

unshift(): Adds one or more elements to the beginning of an array.
    Syntax:
		array.unshift(element1, element2, ...);

	Ex:
		const fruits = ['banana', 'orange'];
		fruits.unshift('apple', 'kiwi');

splice():Adds or removes elements from an array at a specific index.
    Syntax:
		array.splice(startIndex, deleteCount, element1, element2, ...);

	Ex:
        const fruits = ['apple', 'banana', 'orange'];
        fruits.splice(1, 0, 'kiwi'); // Inserts 'kiwi' at index 1

Finding Elements in an Array:

indexOf():Returns the first index at which a given element can be found in the array, or -1 if it is not present.
    Syntax:
		array.indexOf(element);

	Ex:
		const fruits = ['apple', 'banana', 'orange'];
		const index = fruits.indexOf('banana');

lastIndexOf(): Returns the last index at which a given element can be found in the array, or -1 if it is not present.
    Syntax:
		array.lastIndexOf(element);

	Ex:
		const fruits = ['apple', 'banana', 'orange', 'banana'];
		const lastIndex = fruits.lastIndexOf('banana');

includes(): Determines whether an array includes a certain value among its elements, returning true or false as appropriate.
    Syntax:
		array.includes(element);

	Ex:
		const fruits = ['apple', 'banana', 'orange'];
		const isIncluded = fruits.includes('banana');

find():

    Purpose: Returns the first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.
    Syntax:
		array.find(callback);

	Ex:
		const ages = [25, 30, 35, 40];
		const foundAge = ages.find(age => age > 30);

findIndex():Returns the index of the first element in the array that satisfies the provided testing function. Otherwise, -1 is returned.
    Syntax:
		array.findIndex(callback);

	Ex:
        const ages = [25, 30, 35, 40];
        const foundIndex = ages.findIndex(age => age > 30);

Arrow Functions:Arrow functions provide a concise syntax for writing function expressions.
    Syntax:
		const functionName = (param1, param2) => {
			// Function body
		};

	Ex:
		const add = (a, b) => {
			return a + b;
		};

Remove Elements from an Array:

    pop():Removes the last element from an array and returns that element.
    Syntax:
		array.pop();

	Ex:
		const fruits = ['apple', 'banana', 'orange'];
		const removedFruit = fruits.pop();

shift():Removes the first element from an array and returns that element.
    Syntax:
		array.shift();

	Ex:
		const fruits = ['apple', 'banana', 'orange'];
		const removedFruit = fruits.shift();

slice(): Returns a shallow copy of a portion of an array into a new array object selected from start to end.
    Syntax:
		array.slice(startIndex, endIndex);

	Ex:
        const fruits = ['apple', 'banana', 'orange', 'kiwi'];
        const slicedFruits = fruits.slice(1, 3); // Returns ['banana', 'orange']

Difference between splice() and slice():

    splice(): Modifies the original array by adding or removing elements at a specified index.
    slice(): Returns a shallow copy of a portion of an array into a new array without modifying the original array.

Emptying an Array:

Reassign Method: Assign an empty array to the existing array variable.
    Syntax:
	array = [];

	Ex:
		let fruits = ['apple', 'banana', 'orange'];
		fruits = [];

Splice Method: Use splice() with length property to remove all elements from an array.
    Syntax:
		array.splice(0, array.length);

	Ex:
		let fruits = ['apple', 'banana', 'orange'];
		fruits.splice(0, fruits.length);

Array Length Reassign Method: Set the length of the array to 0.
    Syntax:
		array.length = 0;

	Ex:
		let fruits = ['apple', 'banana', 'orange'];
		fruits.length = 0;

Pop Method:Use pop() method in a loop to remove all elements from an array.
    Syntax:
		while (array.length) {
			array.pop();
		}

	Ex:
        let fruits = ['apple', 'banana', 'orange'];
        while (fruits.length) {
            fruits.pop();
        }

Combining Arrays:

concat():Concatenates two or more arrays and returns a new array.
    Syntax:
		const newArray = array1.concat(array2);

	Ex:
		const arr1 = ['apple', 'banana'];
		const arr2 = ['orange', 'kiwi'];
		const combinedArray = arr1.concat(arr2);

spread operator (...): Allows an iterable to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected.
    Syntax:
		const newArray = [...array1, ...array2];

	Ex:
        const arr1 = ['apple', 'banana'];
        const arr2 = ['orange', 'kiwi'];
        const combinedArray = [...arr1, ...arr2];

Iterating an Array:

forEach():Calls a provided function once for each element in an array, in order.
    Syntax:
		array.forEach(callback);

	Ex:
        const fruits = ['apple', 'banana', 'orange'];
        fruits.forEach(fruit => console.log(fruit));

Array Sorting:
sort():Sorts the elements of an array in place and returns the sorted array.
    Syntax:
			array.sort();

	Ex:
		const numbers = [3, 1, 2];
		numbers.sort(); // Returns [1, 2, 3]

reverse():Reverses the elements of an array in place and returns the reversed array.
    Syntax:
		array.reverse();

	Ex:
		const numbers = [1, 2, 3];
		numbers.reverse(); // Returns [3, 2, 1]

Find: The find method is used to retrieve the first element in an array that satisfies a provided condition.
	Syntax:
		array.find(callback(element, index, array));

	Ex:
		const numbers = [10, 20, 30, 40, 50];
		const foundNumber = numbers.find(num => num > 25);
		console.log(foundNumber); // Output: 30

Filter:The filter method creates a new array with all elements that pass the test implemented by the provided function.
    
	Syntax:
		array.filter(callback(element, index, array));

	Ex:
		const numbers = [10, 20, 30, 40, 50];
		const filteredNumbers = numbers.filter(num => num > 25);
		console.log(filteredNumbers); // Output: [30, 40, 50]

Mapping:The map method creates a new array populated with the results of calling a provided function on every element in the calling array.
    
	Syntax:
		array.map(callback(element, index, array));

	Ex:
		const numbers = [1, 2, 3];
		const multipliedNumbers = numbers.map(num => num * 2);
		console.log(multipliedNumbers); // Output: [2, 4, 6]

Reduce:The reduce method applies a function against an accumulator and each element in the array (from left to right) to reduce it to a single value.
    
	Syntax:
    array.reduce(callback(accumulator, currentValue, index, array), initialValue);

	Ex:
		const numbers = [1, 2, 3, 4];
		const sum = numbers.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
		console.log(sum); // Output: 10

Function: A function is a block of reusable code that performs a specific task when called.
	Syntax:
		function functionName(parameters) {
			// Function body
		}

	Ex:
		function greet(name) {
			console.log(`Hello, ${name}!`);
		}

		greet('Alice'); // Output: Hello, Alice!

Function Declaration:Function declarations define functions with the specified name and parameters.
	
	Syntax:
		function functionName(parameters) {
			// Function body
		}

	Ex:
		function add(a, b) {
			return a + b;
		}
		console.log(add(3, 5)); // Output: 8

Function Calling:Function calling refers to the process of invoking a function to execute its code. To call a function, simply use its name followed by parentheses (). 
        
	Syntax: functionName();

    Ex:
	
		function greet(name) {
			console.log(`Hello, ${name}!`);
		}

		greet('Bob'); // Output: Hello, Bob!

Named Function Expression:Named function expressions allow the function to refer to itself within its body.
	
	Syntax:
		const functionName = function() {
			// Function body
		};

	Ex:
		const factorial = function fact(n) {
			return n === 0 ? 1 : n * fact(n - 1);
		};
		console.log(factorial(5)); // Output: 120

Anonymous Function Expression:Anonymous function expressions are functions without a name, often used as callback functions or immediately invoked function expressions (IIFE).
    
	Syntax:
		const functionName = function(parameters) {
			// Function body
		};

	Ex:
		const sum = function(a, b) {
			return a + b;
		};
		console.log(sum(2, 3)); // Output: 5

Constructor Function:Constructor functions are used to create multiple objects of the same type with shared properties and methods.
    
	Syntax:
		function ConstructorName(parameters) {
			this.propertyName = value;
			this.methodName = function() {
				// Method body
			};
		}

	Ex:

	function Person(name, age) {
		this.name = name;
		this.age = age;
		this.greet = function() {
			console.log(`Hello, my name is ${this.name} and I'm ${this.age} years old.`);
		};
	}

	const john = new Person('John', 30);
	john.greet(); // Output: Hello, my name is John and I'm 30 years old.

	   
Javascript Hoisting:
IIFE(Immediately Invoked Function Expression):
default argument object:
rest operator:
function default parameters:

JavaScript Hoisting:
Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their containing scope during the compilation phase. This means that you can call variables and functions before they are declared.

    Ex:
    console.log(myVar); // Output: undefined
    var myVar = 10;

    The myVar variable is hoisted to the top of its scope, so even though it is accessed before its declaration, it does not throw an error. However, its value is undefined until it is assigned 10.

IIFE (Immediately Invoked Function Expression):
An IIFE is a JavaScript function that is declared and invoked immediately after it is defined. It is often used to create a new scope and encapsulate variables to avoid polluting the global scope.

    Syntax:

	(function() {
		// Function body
	})();

	Ex:
		((no1,no2)=>{
			console.log("result: ",no1+no2);
		})(10,20);

Default Argument Object:
The default argument object allows you to specify default values for function parameters if they are not provided by the caller.

    Ex:
    function greet(name = 'Guest') {
        console.log(`Hello, ${name}!`);
    }
    greet(); // Output: Hello, Guest!
    greet('Alice'); // Output: Hello, Alice!
	
	Ex:
	((no1,no2,arguments)=>{
		console.log("result: ",no1+no2+arguments);
	})(10,20,30,40);

Rest Operator: (ES6+ Concept for arguments)
The rest operator (...) allows you to represent an indefinite number of arguments as an array in a function parameter.

    Ex:
    function sum(...numbers) {
        return numbers.reduce((acc, curr) => acc + curr, 0);
    }
    console.log(sum(1, 2, 3)); // Output: 6
    console.log(sum(1, 2, 3, 4, 5)); // Output: 15

Function Default Parameters:
Function default parameters allow you to specify default values for function parameters directly in the function declaration.

    Ex:
	function greet(name = 'Guest') {
		console.log(`Hello, ${name}!`);
	}
	greet(); // Output: Hello, Guest!
	greet('Alice'); // Output: Hello, Alice!
	
Getters and Setters:
Getters and setters are special methods in JavaScript objects that allow you to control access to the properties of an object.

Data Properties vs Accessor Properties:
	Data Properties: These are properties that directly store a value. They can be accessed using dot notation or bracket notation.
    Accessor Properties: These are properties that define a getter function to retrieve the property's value and a setter function to set the property's value.

    Ex:
    const person = {
        firstName: 'John',
        lastName: 'Doe',
        get fullName() {
            return `${this.firstName} ${this.lastName}`;
        },
        set fullName(value) {
            const [firstName, lastName] = value.split(' ');
            this.firstName = firstName;
            this.lastName = lastName;
        }
    };

    console.log(person.fullName); // Output: John Doe

    person.fullName = 'Alice Smith';
    console.log(person.fullName); // Output: Alice Smith

Nullish Operator:
The nullish operator (??) is a logical operator in JavaScript that returns its right-hand operand when its left-hand operand is null or undefined, otherwise, it returns the left-hand operand.

    Ex:
    const defaultValue = 'Default Value';

    const userInput = null;
    const result = userInput ?? defaultValue;
    console.log(result); // Output: Default Value

    const anotherInput = undefined;
    const anotherResult = anotherInput ?? defaultValue;
    console.log(anotherResult); // Output: Default Value

    const actualInput = 'Actual Input';
    const actualResult = actualInput ?? defaultValue;
    console.log(actualResult); // Output: Actual Input

Points:

    Getters and Setters: Understand that getters are used to retrieve the value of a property, while setters are used to set the value of a property.

    Data Properties vs. Accessor Properties: Distinguish between data properties, which directly store a value, and accessor properties, which use getter and setter methods.

    Nullish Operator: Learn that the nullish operator (??) is used to provide a default value when a variable is null or undefined.


Error Handling Concept:Error handling in JavaScript refers to the process of detecting, responding to, and recovering from errors that occur during program execution.

    Try-Catch Statement: In JavaScript, errors can be handled using the try-catch statement. Code that might throw an error is placed inside the try block, and if an error occurs, it is caught and handled in the catch block.

    Syntax:
		try {
			// Code that might throw an error
		} catch (error) {
			// Handle the error
		}
	
	Ex:
		let userData = {
			firstName:"Arunkumar",
			lastName:"Selvam",
			get fullName(){
				return (`${userData.firstName} ${userData.lastName}`)
			},
			set fullName(value){
				if(typeof value != "string"){
					let err = new Error("It is not a string.");
					throw err;
				}
				let values = value.split(" ");
				this.firstName = values[0];
				this.lastName = values[1] ?? "";
			}
		}

		try{
			userData.fullName = 45;
		}
		catch(err){
			alert(err);
		}
		
Error Object:In JavaScript, errors are represented by the Error object or its various subclasses, such as SyntaxError, ReferenceError, TypeError, etc. The Error object contains information about the error, including its message and stack trace.

    Ex:
	
		try {
			throw new Error('Custom error message');
		} catch (error) {
			console.error('An error occurred:', error.message);
		}

Difference Between return and throw:

    return: The return statement is used to end the execution of a function and specify the value to be returned to the caller.

    throw: The throw statement is used to manually generate an error (i.e., throw an exception) in JavaScript code.

    Usage:
        Use return to exit a function and return a value.
        Use throw to raise an error condition and trigger error handling.

	Ex:
		function divide(a, b) {
			if (b === 0) {
				throw new Error('Cannot divide by zero');
			}
			return a / b;
		}

		try {
			const result = divide(10, 0);
			console.log('Result:', result);
		} catch (error) {
			console.error('An error occurred:', error.message);
		}

Scopes:

    Definition: Scope refers to the visibility and accessibility of variables in JavaScript. There are two main types of scope: global scope and local scope.

    Global Scope: Variables declared outside of any function have global scope and can be accessed from anywhere in the code.

    Local Scope: Variables declared inside a function have local scope and can only be accessed within that function.


