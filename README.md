# JavaScript-Basics

__Topics__

1. Variables
2. Datatypes
3. Objects
4. Arrays
5. Operators
7. Conditional Statements
8. Looping
9. OOPs in JavaScript
10. Factory Function
11. Constructor Function
12. Object Inbuilt Functions
13. Object Cloning
14. Spread Operator
15. Math Object, String Object, Date Object
16. Arrow Function
17. Array Inbuilt Functions(Add, Find, Remove, Emptying, Iterating, Sorting, Mapping, Reduce)
19. Functions (Function Declaration, Function Expression)
20. JavaScript Hoisting
21. IIFE (Immediately Invoked Function Expression)
22. Arguments and Parameter
23. Rest Operators
24. Default Parameters
25. Getters and Setters
26. Javascript Scopes
27. Error Handling(try, throw catch), Error Object
28. Promises
29. Async and Await
30. Fetch API


